import React from 'react';
import '../App.css';
import DashboardMessage from '../Functional/DashboardMessage'

function Dashboard() {
    return (
        <div className="Dashboard">
            < DashboardMessage />
        </div>
    );
};

export default Dashboard;
