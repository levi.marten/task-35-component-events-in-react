import React from 'react';
import '../App.css';
import RegisterForm from '../Forms/RegisterForm'

function Register() {

  const handleRegisterClicked = () => {
   // Update messages on screen? 
  }

  return (
    <div className="Register">
      <h1>Welcome!</h1>
      <h2>Register to SurveyPuppy</h2>
      <RegisterForm registerClicked={ handleRegisterClicked } />
    </div>
  );
};

export default Register;
